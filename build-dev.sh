# BUILD AND TAG DOCKER IMAGE
docker build -f "Dockerfile-dev" -t "react:dev" .
# RUN CONTAINER
docker run -v ${PWD}:/app -v /app/node_modules -p 3001:3000 --rm react:dev